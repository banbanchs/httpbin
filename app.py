#!/usr/bin/env python2
#!coding=utf-8

import tornado.ioloop
import tornado.httpserver
import tornado.web
from tornado.options import options, define

from httpbin.core import *

define('port', default=5000, help='run on the given port', type=int)

app = tornado.web.Application([
    (r'/get', GetHandler),
    (r'/post', PostHandler),
    (r'/headers', HeadersHandler),
    (r'/user-agent', UserAgentHandler),
    (r'/cookies', CookiesHandler),
    (r'/cookies/set', SetCookiesHandler),
    (r'/cookies/set/(\w+)/(\w+)', SetCookiesByUriHandler),
    (r'/cookies/delete', DeleteCookieHandler),
    (r'/redirect/(\d+)', RedirectNTimesHandler),
    (r'/redirect-to', RedirectToHandler),
    (r'/response-headers', ResponseHeadersHandler),
    (r'/delay/(\d+)', DelayHandler),
    (r'/ip', IpHandler),
], debug=True)


if __name__ == '__main__':
    options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
