#!/usr/bin/env python2
#!coding=utf-8

import json
import time

import tornado.web
from tornado.escape import json_encode


class BaseHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header('Content-Type', 'application/json')

    @staticmethod
    def convert_petty_json(dictionary):
        json_str = json_encode(dictionary)
        parsed = json.loads(json_str)
        return json.dumps(parsed, indent=2, sort_keys=True)


class GetHandler(BaseHandler):
    def get(self):
        data = {
            "url": self.request.full_url(),
            "headers": self.request.headers,
            "origin": self.request.remote_ip
        }
        self.write(self.convert_petty_json(data))


class PostHandler(BaseHandler):
    def post(self):
        post_data = {}
        for (name, value) in self.request.arguments.items():
            post_data[name] = value[0]
        data = {
            "url": self.request.full_url(),
            "headers": self.request.headers,
            "origin": self.request.remote_ip,
            "json": post_data
        }
        self.write(self.convert_petty_json(data))


class HeadersHandler(BaseHandler):
    def get(self):
        self.write(self.convert_petty_json(self.request.headers))


class UserAgentHandler(BaseHandler):
    def get(self):
        data = {
            "User-Agent": self.request.headers.get('User-Agent')
        }
        self.write(self.convert_petty_json(data))


class IpHandler(BaseHandler):
    def get(self):
        data = {
            "origin": self.request.remote_ip
        }
        self.write(self.convert_petty_json(data))


class CookiesHandler(BaseHandler):
    def get(self):
        cookies = {}
        for cookie in self.cookies.keys():
            cookies[cookie] = self.get_cookie(cookie)
        data = {
            "Cookies": cookies
        }
        self.write(self.convert_petty_json(data))


class SetCookiesHandler(BaseHandler):
    def get(self):
        for (name, values) in self.request.arguments.items():
            for value in values:
                self.set_cookie(name, value)
        self.redirect(r'/cookies')


class SetCookiesByUriHandler(BaseHandler):
    def get(self, name, value):
        self.set_cookie(name, value)
        self.redirect(r'/cookies')


class DeleteCookieHandler(BaseHandler):
    def get(self):
        for name in self.request.arguments.keys():
            self.clear_cookie(name)
        self.redirect(r'/cookies')


class RedirectNTimesHandler(BaseHandler):
    def get(self, times):
        times = int(times)
        assert times > 0
        if times == 1:
            self.redirect(r'/get')
        self.redirect(r'/redirect/%s' % str(times - 1))


class RedirectToHandler(BaseHandler):
    def get(self):
        redirect_url = self.get_argument("url", "http://example.com/")
        self.redirect(redirect_url, True, 302)


class ResponseHeadersHandler(BaseHandler):
    # bugs?
    def get(self):
        data = {
            "Content-Type": "application/json",
            # "Content-Length": self.request.headers.get('Content-Length')
        }
        for (name, value) in self.request.arguments.items():
            data[name] = value[0]
        self.write(self.convert_petty_json(data))


class DelayHandler(BaseHandler):
    def get(self, seconds):
        seconds = min(int(seconds), 10)
        time.sleep(seconds)
        data = {
            "args": self.request.arguments,
            "origin": self.request.remote_ip,
            "url": self.request.full_url(),
            "headers": self.request.headers
        }
        self.write(self.convert_petty_json(data))

